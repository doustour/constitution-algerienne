# Constitution Algérienne

Constitutions de la République Algérienne Démocratique et Populaire.

* Branche [Constitution de 1963](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_de_1963)
* Branche [Constitution de 1976](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_of_1976)
* Branche [Constitution de 1989](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_of_1989)
* Branche [Constitution de 1996](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_of_1996)
* Branche [Constitution de 2002](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_of_2002)
* Branche [Constitution de 2008](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_of_2008)
* Branche [Constitution de 2016](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_of_2016)
* Branche [Constitution de 2020](https://gitlab.com/BoFFire/constitution-algerienne/tree/Constitution_of_2020)